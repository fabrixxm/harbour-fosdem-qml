import QtQuick 2.2
import Sailfish.Silica 1.0

Item {
    id: topNotification
    property bool isPublished: false

    property string summary: ""
    property string body: ""

    y: -height
    height: (Theme.paddingSmall * 2) + (txtSummary.height * txtSummary.visible) + (txtBody.height * txtBody.visible)
    width: parent.width

    Rectangle {
        color: Theme.overlayBackgroundColor
        anchors.fill: parent
    }

    Label {
        id: txtSummary
        text: summary
        wrapMode: Text.WrapAnywhere
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
        visible: summary !== ""
    }

    Label {
        id: txtBody
        text: body
        font.pixelSize: Theme.fontSizeTiny
        wrapMode: Text.WrapAnywhere
        anchors {
            top: txtSummary.bottom
            left: parent.left
            right: parent.right
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingMedium
            rightMargin: Theme.paddingMedium
        }
        visible: body !== ""
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            isPublished = false;
        }
    }



    function error(traceback) {
        summary = qsTr("Error");
        if (isPublished) {
            body = body + "\n-------------------\n" + traceback;
        } else {
            body = qsTr("An error has occured: %1").arg(traceback);
        }
        publish();
    }

    function publish() {
        isPublished = true;
    }



    states: [
        State {
            name: "hidden"
            when: !isPublished
            PropertyChanges { target: topNotification; y: -height; }
        },
        State {
            name: "published"
            when: isPublished
            PropertyChanges { target: topNotification; y: 0; }
        }
    ]

    transitions: Transition {
        NumberAnimation { properties: "y"; easing.type: Easing.InOutQuad }
    }
}
