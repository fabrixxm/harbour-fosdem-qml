import QtQuick 2.0
import Sailfish.Silica 1.0

PullDownMenu {
    MenuItem {
        id: aboutMenu
        text: qsTr("About")
        onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
    }
    MenuItem {
        id: importMenu
        text: qsTr("Import")
        onClicked: pageStack.push(Qt.resolvedUrl("DownloadDialog.qml"));
        visible: true
    }
    MenuItem {
        id: guestbookMenu
        text: qsTr("Guest book")
        onClicked: pageStack.push(Qt.resolvedUrl("GuestBook.qml"));
    }
    MenuItem {
        id: mapMenu
        text: qsTr("Map")
        onClicked: pageStack.push(Qt.resolvedUrl("Map.qml"));
    }
}
