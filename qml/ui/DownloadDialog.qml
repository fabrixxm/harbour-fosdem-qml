import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: dialog

    Column {
        width: parent.width

        spacing: Theme.paddingLarge

        DialogHeader {
            title: qsTr("Download")
        }

        Label {
            text: qsTr("All 'Checked' entries will be deleted!")
            font.bold: true
            width: parent.width - 2 * x
            x: Theme.horizontalPageMargin
        }

        TextField {
            id: urlField
            text: {
                var year = new Date().getFullYear();
                return "https://fosdem.org/" + year + "/schedule/xml"
            }
            width: parent.width
            label: qsTr("URL of Fosdem Pentabarf XML")
            labelVisible: true

        }

    }

    onDone: {
        if (result == DialogResult.Accepted) {
            var url = urlField.text;
            py.call("backend.download_file", [url], download_end);
        }
    }

}

/*
Component {
    Dialog {
        id: dialog
        title: i18n.tr("Download")
        text: i18n.tr("Url of Fosdem Pentabarf XML:")

        property real value: 0.0
        property bool progress_visible: false

        LabelBase {
            text: i18n.tr("All 'Checked' entries will be deleted!")
        }

        TextField {
            id: url
            text: {
                var year = new Date().getFullYear();
                return "https://fosdem.org/" + year + "/schedule/xml"
            }
            width: parent.width
            onAccepted: downloadButton.clicked();
        }

        ProgressBar {
            id: progress
            width: parent.width
            value: dialog.value
            visible: dialog.progress_visible
        }

        Button {
            id: downloadButton
            text: i18n.tr("Download")
            color: UbuntuColors.blue
            visible: !dialog.progress_visible

            onClicked: {
                //console.log(url.text);
                url.focus = false;
                Qt.inputMethod.hide();
                py.call("backend.download_file", [url.text], download_end);
            }
        }

        Button {
            //anchors.right: parent.right
            //visible: true

            text: i18n.tr("Cancel")
            onClicked: {
                PopupUtils.close(download_dialog.current);
                //console.log("Cancel");
            }
        }
    }
}
*/

