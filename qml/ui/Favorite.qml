import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property bool checked
    width: Theme.iconSizeMedium
    height: Theme.iconSizeMedium

    Image {
        anchors{
            verticalCenter: parent.verticalCenter
            fill: parent
        }       

        source: checked ? "image://theme/icon-m-favorite-selected"  :"image://theme/icon-m-favorite"
    }
}
