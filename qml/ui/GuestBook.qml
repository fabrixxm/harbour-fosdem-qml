import QtQuick 2.0
import Sailfish.Silica 1.0

// Instructions: follow these simple steps.
//
//  1. Find GuestBook.qml (the file you're reading!) in the Sailfish IDE.
//  2. Make a copy of a GuestBookEntry components.
//  3. Edit your name and comment to your liking.
//  4. Save the file (Ctrl-S).
//
//  Watch as QML Live automatically updates the app in realtime.

Page {
    id: page

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge

        Column {
            id: col
            spacing: Theme.paddingLarge
            width: parent.width

            Image { source: Qt.resolvedUrl("../images/lom.png") }

            PageHeader { title: "FOSDEM'23 Guestbook" }

            Label {
                text: "Add your name to the Linux on Mobile FOSDEM'23 Guestbook"
                x: Theme.horizontalPageMargin
                width: parent.width - 2 * x
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.secondaryHighlightColor
            }

            Instructions {}

            SectionHeader { text: "I was there!" }

            Column {
                width: parent.width

                // Add a new entry here by copying a GuestBookEntry and editing it
                // ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓

                GuestBookEntry {
                    name: "David Llewellyn-Jones"
                    comment: "Writing this in advance!"
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
