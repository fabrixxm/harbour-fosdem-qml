import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Silica.private 1.0
import QtQuick.XmlListModel 2.0

TabItem {
    id: track
    property var trackModel: ListModel {}
    property string date: ""
    property var scheduleModel: ListModel {}
    property real topMargin
    property alias xmlsource: daymodel.source
    property int dayIndex

    anchors.fill: parent
    flickable: flickable

    SilicaListView {
        id: flickable

        anchors.fill: parent

        CommonPullDown {
        }

        header: PageHeader {
            _titleItem.y: topMargin
            title: qsTr("Tracks")
            description: date
        }

        VerticalScrollDecorator {}

        model: track.trackModel
        delegate: trackDelegate

        ViewPlaceholder {
            enabled: daymodel.count === 0 && daymodel.status != XmlListModel.Loading
            text: qsTr("Empty database")
            hintText: qsTr("Select Import from the menu to load the schedule")
        }

        BusyIndicator {
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: daymodel.status == XmlListModel.Loading
        }
    }

    Connections {
        target: daymodel

        onStatusChanged: {
            if (daymodel.status == XmlListModel.Ready) {
                updateTrackModel()
            }
        }
    }

    Connections {
        target: mainView

        onXmlChanged: {
            daymodel.reload()
        }
    }


    Component {
        id: trackDelegate

        ListItem {
            id: delegate
            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: Math.max(dayLabel.height, Theme.itemSizeSmall)

            Label {
                id: dayLabel
                text: model.title
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter

                leftPadding: Theme.horizontalPageMargin
                rightPadding:  Theme.horizontalPageMargin
                bottomPadding: Theme.paddingMedium
                topPadding: Theme.paddingMedium

                color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                wrapMode: Text.WordWrap
                verticalAlignment: Text.AlignVCenter
            }

            onClicked: {
                var path = [daymodel.get(track.dayIndex).date, model.title]
                scheduleModel.clear()
                py.call("backend.find_events_by_day_track", path, function(events) {
                    for (var i=0; i < events.length; i++) {
                        console.log("Event: " + events[i].title)
                        scheduleModel.append(events[i]);
                    }
                    pageStack.push(Qt.resolvedUrl("Schedule.qml"), {
                                       "model": scheduleModel,
                                       "track": model.title,
                                   })
                })
            }
        }
    }

    XmlListModel {
        id: daymodel

        query: "/schedule/day"

        XmlRole {
            name: "date"
            query: "@date/string()"
        }
    }

    function updateTrackModel() {
        trackModel.clear();
        date = daymodel.get(track.dayIndex).date

        var path = [daymodel.get(track.dayIndex).date]
        py.call("backend.find_tracks_by_day", path, function(tracks) {
            for (var i=0; i < tracks.length; i++) {
                trackModel.append(tracks[i]);
            }
        })
    }
}
