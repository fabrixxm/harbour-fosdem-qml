import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: schedule

    property var model: ListModel {}
    property string track: ""

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Schedule")
            description: track
        }

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                id: mapMenu
                text: qsTr("Map")
                onClicked: pageStack.push(Qt.resolvedUrl("Map.qml"));
            }
        }

        model: schedule.model
        delegate: scheduleDelegate

        ViewPlaceholder {
            enabled: schedule.model.count === 0
            text: qsTr("'No schedule info")
        }
    }

    Component {
        id: scheduleDelegate

        ListItem {
            id: delegate
            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: row.height + (Theme.paddingMedium * 2)

            Row {
                id: row
                spacing: Theme.paddingSmall

                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    topMargin: Theme.paddingSmall
                    left: parent.left
                    right: parent.right
                    top:parent.top
                }

                Column {
                    id: column
                    spacing: Theme.paddingSmall
                    width: parent.width - lecture_favourite.width - Theme.paddingSmall

                    Label {
                       text: model.title
                       width: parent.width
                       wrapMode: Text.WordWrap
                       color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                    }

                    Label {
                        text: "[" + toDateString(day) +"] " + start + " - " + end + " " + qsTr('in') + " " + room
                        width: parent.width
                        wrapMode: Text.WordWrap
                        color: delegate.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                        font.pixelSize: Theme.fontSizeTiny
                    }
                }

                Favorite {
                    id: lecture_favourite
                    width: Theme.iconSizeMedium
                    height: width
                    checked: lecture_checked

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            py.call('backend.toggle', [model], function (data) {
                                model.lecture_checked = data;
                                //lecture_checked.checked = data;
                            });
                            mainView.favouritesChanged()
                        }
                    }
                }

            }
            onClicked: {
                pageStack.push(Qt.resolvedUrl("Lecture.qml"), {"model": schedule.model.get(index)})
            }
        }
    }
}


/*
    header: CommonHeader {
        title: i18n.tr('Schedule')
        flickable: scheduleListView
    }

    property var model: ListModel {}

    ListView {
        id: scheduleListView
        anchors.fill: parent
        model: schedule.model
        delegate: scheduleDelegate
    }

    Component {
        id: scheduleDelegate

        ListItem {
            divider.visible: false
            highlightColor: hlColor

            ListItemLayout {
                id: layout
                title.text: model.title
                subtitle.text: "[" + day +"] " + start + " - " + end + " " + i18n.tr('in') + " " + room

                Favorite {
                    id: saved_lecture
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Leading
                    checked: lecture_checked
                }

                ProgressionSlot {}
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("Lecture.qml"), {"model": schedule.model.get(index)})
            }
        }
    }

    Loader {
        id: emptyStateLoader
        width: parent.width
        anchors.centerIn: parent
        active: schedule.model.count === 0
        sourceComponent: Label {
            text: i18n.tr('No schedule info')
            horizontalAlignment: Text.AlignHCenter
            textSize: Label.Medium
        }
    }
}
*/
