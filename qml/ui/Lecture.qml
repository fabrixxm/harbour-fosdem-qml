import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: lecture
    property int event_id: 0
    property var model: ListModel {}
    property var overlaps: ListModel {}
    property bool favouriteChanged

    function set_lecture(item) {
        lecture.model = item
        lecture.event_id = item.id

        lecture_start.text = item.start
        lecture_end.text = item.end
        lecture_room.text = item.room
        lecture_title.text = item.title
        lecture_subtitle.text = item.subtitle
        lecture_abstract.text = item.abstract
        lecture_description.text = item.description
        lecture_persons.text = item.persons
        lecture_checked.checked = item.lecture_checked

        countOverlap();
    }

    function countOverlap() {
        lecture.overlaps.clear()
        py.call('backend.get_overlap', [lecture.model], function (events) {
            for (var i=0; i < events.length; i++) {
                lecture.overlaps.append(events[i]);
            }
        });
    }

    Component.onCompleted: {
        set_lecture(lecture.model)
        favouriteChanged = false
    }

    onStatusChanged: {
        if (status == PageStatus.Inactive) {
            console.log("STATUS: Inactive")
            if (favouriteChanged) {
                console.log("STATUS: Favourite changed")
                mainView.favouritesChanged()
            }
        }
    }

    SilicaFlickable {
        contentHeight: mainCol.childrenRect.height

        PullDownMenu {
            MenuItem {
                id: mapMenu
                text: qsTr("Map")
                onClicked: pageStack.push(Qt.resolvedUrl("Map.qml"));
            }
        }

        anchors.fill: parent

        Column {
            spacing: Theme.paddingMedium
            anchors.fill: parent

            PageHeader {
                width: parent.width
                title: qsTr('Lecture')
            }

            Column {
                id: mainCol
                width: parent.width - 2 * Theme.horizontalPageMargin
                x: Theme.horizontalPageMargin

                spacing: Theme.paddingMedium

                Row {
                    width: parent.width
                    spacing: Theme.paddingSmall

                    Label {
                        id: lecture_title
                        font.bold: true
                        width: parent.width - lecture_checked.width - Theme.paddingSmall
                        height: contentHeight > lecture_checked.height ? contentHeight : lecture_checked.height
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }

                    Favorite {
                        id: lecture_checked
                        width: Theme.iconSizeMedium
                        height: width

                        MouseArea {
                            anchors.fill: parent

                            onClicked: {
                                py.call('backend.toggle', [lecture.model], function (data) {
                                    lecture.model.lecture_checked = data;
                                    lecture_checked.checked = data;

                                    countOverlap();
                                    favouriteChanged = true
                                });
                            }
                        }
                    }
                }


                Row {
                    width: parent.width
                    spacing: Theme.paddingSmall

                    visible: lecture.overlaps.count > 0

                    Label {
                        id: overlap_label
                        font.pixelSize: Theme.fontSizeTiny
                        text: qsTr("Overlaps with %1 checked lectures:\n").arg(lecture.overlaps.count) + get_list_text()
                        width: parent.width - overlap_button.width - Theme.paddingSmall
                        height: contentHeight > overlap_button.height ? contentHeight : overlap_button.height
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap

                        function get_list_text() {
                            var t = [];
                            for(var k=0; k<lecture.overlaps.count; k++) {
                                var e = lecture.overlaps.get(k);
                                t.push(e.title + qsTr(' in ') + e.room);
                            }
                            return t.join("\n");
                        }
                    }



                    IconButton {
                        width: Theme.iconSizeMedium
                        height: width

                        id: overlap_button
                        icon.source: "image://theme/icon-m-data-traffic?"+ (pressed
                                                                           ? Theme.highlightColor
                                                                           : Theme.primaryColor)

                        onClicked: pageStack.push(Qt.resolvedUrl("Overlaps.qml"), {"item": lecture.model})

                    }

                }

                Grid {
                    width: parent.width
                    columns: 2
                    rows: 5
                    spacing: Theme.paddingMedium

                    ///
                    Label {
                        text: qsTr('Time')
                        color: Theme.secondaryColor
                        font.bold: true
                    }
                    Row {
                        width: parent.width

                        Label {
                            id: lecture_start
                        }

                        Label { text: "-" }

                        Label {
                            id: lecture_end
                        }
                    }

                    ///
                    Label {
                        text: qsTr('Room')
                        color: Theme.secondaryColor
                        font.bold: true
                    }

                    Label {
                        id: lecture_room
                    }

                    ///
                    Label {
                        text: qsTr('Speaker')
                        color: Theme.secondaryColor
                        font.bold: true
                    }
                    Label {
                        id: lecture_persons
                    }
                }

                Label {
                    id: lecture_subtitle
                    width: parent.width
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: lecture_abstract
                    width: parent.width
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: lecture_description
                    width: parent.width
                    wrapMode: Text.WordWrap
                }
            }
        }

        ViewPlaceholder {
            enabled: !lecture.model || lecture.model.count === 0
            text: qsTr("'No lecture info")
        }

        VerticalScrollDecorator {}
    }

}
/*
    header: CommonHeader {
        title: i18n.tr('Lecture')
        flickable: lectureFlickable
    }

    property int event_id: 0
    property var model: ListModel {}
    property real itemName: units.gu(7)
    property real itemTime: units.gu(8)

    function set_lecture(item) {
        lecture.model = item
        lecture.event_id = item.id

        lecture_start.text = item.start
        lecture_end.text = item.end
        lecture_room.text = item.room
        lecture_title.text = item.title
        lecture_subtitle.text = item.subtitle
        lecture_abstract.text = item.abstract
        lecture_description.text = item.description
        lecture_persons.text = item.persons
        lecture_checked.checked = item.lecture_checked
    }

    Component.onCompleted: set_lecture(lecture.model);

    Flickable {
        id: lectureFlickable
        contentHeight: contentItem.childrenRect.height

        anchors {
            fill: parent
            leftMargin: gnalMargins
            rightMargin: gnalMargins
            bottomMargin: gnalMargins
        }

        Column {
            id: mainCol
            width: parent.width
            spacing: gnalSpacing

            anchors {
                top: parent.top
                topMargin: gnalMargins
            }

            Row {
                width: parent.width
                spacing: gnalSpacing

                LabelBase {
                    id: lecture_title
                    font.bold: true
                    width: parent.width - lecture_checked.width - gnalSpacing
                    verticalAlignment: Text.AlignVCenter
                }

                Favorite {
                    id: lecture_checked
                    width: units.gu(3)
                    height: width

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            py.call('backend.toggle', [lecture.model], function (data) {
                                lecture.model.lecture_checked = data;
                                lecture_checked.checked = data;
                            });
                        }
                    }
                }
            }

            Row {
                width: parent.width
                spacing: gnalSpacing

                LabelBase {
                    text: i18n.tr('Time')
                    width: itemName
                    font.bold: true
                }

                LabelBase {
                    id: lecture_start
                    width: itemTime
                }

                LabelBase {
                    id: lecture_end
                    width: itemTime
                }
            }

            Row {
                width: parent.width
                spacing: gnalSpacing

                LabelBase {
                    text: i18n.tr('Room')
                    width: itemName
                    font.bold: true
                }

                LabelBase {
                    id: lecture_room
                    width: itemName * 2
                }
            }

            Row {
                width: parent.width
                spacing: gnalSpacing

                LabelBase {
                    text: i18n.tr('Speaker')
                    width: itemName
                    font.bold: true
                }

                LabelBase {
                    id: lecture_persons
                    width: itemName * 2
                }
            }

            LabelBase {
                id: lecture_subtitle
            }

            LabelBase {
                id: lecture_abstract
            }

            LabelBase {
                id: lecture_description
            }
        }
    }

    Loader {
        id: emptyStateLoader
        width: parent.width
        anchors.centerIn: parent
        active: lecture.model.count === 0
        sourceComponent: Label {
            text: i18n.tr('No lecture info')
            horizontalAlignment: Text.AlignHCenter
            textSize: Label.Medium
        }
    }
}
*/

