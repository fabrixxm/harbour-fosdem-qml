import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    property alias name: nameLabel.text
    property string comment: "Find us at the Linux on Mobile stand\nBuilding H Stand 6\n\nThen:\n\t1. Find GuestBook.qml in Sailfish IDE\n\t2. Make a copy of a GuestBookEntry\n\t3. Edit the name and comment\n\t4. Save the file (Ctrl-S)\n\nWatch as QML Live automatically updates the app in realtime"

    width: parent.width
    contentHeight: col.height + 2 * Theme.paddingLarge

    Column {
        id: col
        spacing: Theme.paddingSmall
        width: parent.width
        y: Theme.paddingLarge

        Label {
            id: nameLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            text: "Instructions"
        }

        Label {
            id: commentLabel
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.secondaryColor
            truncationMode: TruncationMode.Fade
            maximumLineCount: 1
            text: "Press me for instructions"
        }
    }

    onClicked: {
        pageStack.animatorPush(Qt.resolvedUrl("Comment.qml"), {
                                   name: "Follow these simple steps",
                                   comment: comment,
                                   title: "Instructions"
                               })
    }
}
