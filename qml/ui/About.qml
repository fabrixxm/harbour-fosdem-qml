import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: about
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: col.height

        Column {
            id: col

            width: parent.width
            anchors {
                top: parent.top
                leftMargin: Theme.paddingLarge
                rightMargin: Theme.paddingLarge
                topMargin: Theme.paddingLarge
            }

            spacing: Theme.paddingLarge

            Label {
                width: parent.width
                horizontalAlignment: Text.Center
                text: qsTr("Fosdem QML")
                font.pixelSize: Theme.fontSizeExtraLarge
            }

            Image {
                width: parent.width
                fillMode: Image.PreserveAspectFit
                horizontalAlignment: Image.AlignHCenter
                source: "../images/icon.svg"
                sourceSize.height: 300
            }


            Label {
                width: parent.width
                horizontalAlignment: Text.Center
                visible: false
                text: "Version"
                Component.onCompleted: {
                    py.importModule("backend", function() {
                        text = qsTr("Version %1").arg(py.evaluate("backend.__version__"));
                        visible = true;
                    })
                }
            }

            Separator {
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
                color: Theme.secondaryColor
            }

            Label {
                text: qsTr("Author: Josip Delic")
                horizontalAlignment: Text.Center
                width: parent.width
            }

            Label {
                text: qsTr("Redesign: Joan CiberSheep")
                horizontalAlignment: Text.Center
                width: parent.width
            }

            Label {
                text: qsTr("Sailfish OS Port: Fabio Comuni")
                horizontalAlignment: Text.Center
                width: parent.width
            }

            Label {
                text: qsTr("2023 update: David Llewellyn-Jones")
                horizontalAlignment: Text.Center
                width: parent.width
            }

            Label {
                //TRANSLATORS: Here %1 is FOSDEM name (Should read as 'Official FOSDEM website')
                text: qsTr("Official %1 website").arg("<a href='https://fosdem.org/'>FOSDEM</a>")
                onLinkActivated: Qt.openUrlExternally(link)
                horizontalAlignment: Text.Center
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                text: qsTr("Source code available on <a href='https://gitlab.com/flypig/harbour-fosdem-qml'>GitLab</a>")
                onLinkActivated: Qt.openUrlExternally(link)
                horizontalAlignment: Text.Center
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                text: qsTr("Powered by <b>PyOtherSide</b>")
                horizontalAlignment: Text.Center
                width: parent.width
            }


        }

        VerticalScrollDecorator {}
    }

}
