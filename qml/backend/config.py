import os

def _get_xdg_cache_dir(app_id):
    """
    Return the XDG cache directory.
    See https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
    """
    cache_dir = os.environ.get('XDG_DATA_HOME')
    if not cache_dir:
        cache_dir = os.path.expanduser('~/.local/share')
        if cache_dir.startswith('~/'):  # Expansion failed.
            cache_dir = os.environ.get('HOME') # If expansion fail, this will fail too, probably..
    return os.path.join(cache_dir, app_id)


APP_ID = "harbour-fosdem-qml"
DIR = _get_xdg_cache_dir(APP_ID)
FILENAME = os.path.join(DIR, "schedule.xml")
DB_FILE = os.path.join(DIR, "saved.db")
