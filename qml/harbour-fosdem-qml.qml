import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "ui"


ApplicationWindow {
    objectName: "mainView"
    id: mainView

    property var path: []

    property string xmlsource
    signal xmlChanged
    signal favouritesChanged

    function download_end(data) {
        // XXX should we rather send signals?
        progressBar.visible = false

        py.call("backend.get_schedule_file_path",  [true], function(path) {
            xmlsource = path;
            console.log("Data downloaded")
            xmlChanged();
        })
    }

    function load_schedule(exists) {
        console.log("load_schedule", exists);
        // this is called when app starts!
        // XXX should we rather send signals?
        if (!exists) {
            var dialog = pageStack.push(Qt.resolvedUrl("ui/DownloadDialog.qml"));
        }
        else {
            py.call("backend.get_schedule_file_path",  [], function(path) {
                xmlsource = path;
                xmlChanged();
            })
        }
    }

    initialPage: Component { MainPage {} }
    cover: Qt.resolvedUrl("CoverPage.qml")

    Python {
        id: py
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('backend', function() {
                console.log("DEBUG: python loaded");
            });

            setHandler('on-progress', function(value) {
                if (!progressBar.visible) {
                    progressBar.visible = true;
                }
                progressBar.value = Math.ceil(value * 2);
            });
        }

        onError: {
            console.log('Error: ' + traceback);
            errorDialog.error(traceback);
        }
    }

    ProgressBar {
        id: progressBar
        minimumValue: 0
        maximumValue: 100
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        visible: false
    }

    TopNotification {
        id: errorDialog
    }

    Component.onCompleted: {
        py.call("backend.file_exists", [], load_schedule)
    }

    function toDateString(datestr) {
        return toDate(datestr).toDateString()
    }

    function toDate(datestr) {
        return new Date(datestr);
    }

    function now() {
        return new Date();
    }
}
