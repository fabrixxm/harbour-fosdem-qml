import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover
    property var model: ListModel {}

    Component.onCompleted: {
        update();
    }

    Label {
        id: titleText
        text: qsTr("FOSDEM")
        x: Theme.paddingLarge
        width: parent.width - (2 * Theme.paddingLarge)
        fontSizeMode: Text.VerticalFit
        font.pixelSize: Theme.fontSizeLarge
        anchors {
            top: parent.top
            topMargin: Theme.paddingLarge
        }
        color: Theme.highlightColor
    }

    Image {
        source: "images/cover-fosdem.svg"
        width: parent.width

        fillMode: Image.PreserveAspectFit
        horizontalAlignment: Image.AlignHCenter
    }

    SilicaListView {
        anchors {
            left: parent.left
            right: parent.right
            top: titleText.bottom
            bottom: parent.bottom
            topMargin: Theme.paddingSmall
            leftMargin: Theme.paddingLarge
            rightMargin: Theme.paddingLarge
            bottomMargin: Theme.itemSizeSmall * 2
        }
        model: cover.model
        delegate: coverDelegate
    }


    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: "image://theme/icon-cover-next"
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("ui/Lecture.qml"), {"model": cover.model.get(0)}, PageStackAction.Immediate)
                mainView.activate();
            }
        }
    }

    Component {
        id: coverDelegate
        ListItem {
            id: delegate

            anchors {
                left: parent.left
                right: parent.right
            }
            contentHeight: col.height + (Theme.paddingSmall * 2)
            Column {
                id: col
                width: parent.width
                anchors {
                    left: parent.left
                    right: parent.right
                    top:parent.top
                }

                Label {
                   text: model.title
                   width: parent.width
                   truncationMode: TruncationMode.Fade
                   color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                   font.pixelSize: Theme.fontSizeSmall
                }

                Label {
                   text: "" + toDateString(day).split(" ")[0] + " " + start + " " + qsTr('in') + " " + room
                   width: parent.width
                   truncationMode: TruncationMode.Fade
                   color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
                   font.pixelSize: Theme.fontSizeTiny
                }

            }
        }
    }

    Connections {
        target: mainView

        onFavouritesChanged: cover.update()
    }

    function update() {
        cover.model.clear();
        py.call("backend.select_next", [], function (events) {
            if (!events) return;
            for (var i=0; i < events.length; i++) {
                //console.log(events[i]);
                cover.model.append(events[i]);
            }
        });
    }

    Timer {
        interval: 1000 * 60 // one minute
        repeat: true
        running: true
        onTriggered: update()
    }
}
